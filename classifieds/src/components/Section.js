import React from 'react';
import './Section.scss';
import {List, Map} from 'immutable';


export default class Section extends React.Component {
	
	constructor(props){
		super(props);
	}
	
	getSection(){
		return this.props.section || {};
	}
	
	
	//Each Section uses a particular layout
	render() {
		return <div className="Section">
		
			{this.getSection().section_title ?
				<h5>{this.getSection().section_title}</h5>:
			null}
			
			{(this.getSection().section_type == 'layout1') ?
				<div>
					<h1><a href={this.getSection().link}>{this.getSection().title}</a></h1>
					<div className="row">
						<div className="col-xs-7">
							<p>{this.getSection().post}</p>
						</div>
						<div className="col-xs-5">
							<a href={this.getSection().link}>
								<img src={this.getSection().image} />
							</a>
						</div>						
					</div>
				</div>:
			null}
			
			{(this.getSection().section_type == 'layout2') ?
				<ul>
					{this.getSection().links ? 
						this.getSection().links.map((single, i) =>
							<li key={i}><span><a href={single.link}>{single.text}</a></span></li>
						):
					null}
				</ul>:
			null}
			
			{(this.getSection().section_type == 'layout3') ?
				<a href={this.getSection().link}>
					<img src={this.getSection().image} />
				</a>:
			null}
			
			{(this.getSection().section_type == 'layout4') ?
				<div>
					{this.getSection().links ?
						this.getSection().links.map((single, i) =>
							<p key={i} className="small"><a href={single.link}>{single.text}</a></p>
						):
					null}
					<img style={{width: '75%'}} src={this.getSection().image} />
				</div>:
			null}
			
			
			<div className="clearfix" />
			
			{(this.getSection().section_type == 'break') ?
				<div className="hidden-xs divider" />:
				<div className="visible-xs divider" />
			}
		</div>;
	
	}

}