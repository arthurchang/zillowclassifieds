import React from 'react';
import 'bootstrap';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import './App.scss';
import Section from './components/Section';
import {List, Map, fromJS} from 'immutable';

//load data for classifieds
import data from './data.js';

class App extends React.Component {

	constructor(props){
		super(props);
		
		//load the data into the state
		//set at the first tab
		let name = data.tabs[0];
		this.state = {
			selectedTab: name,
			sections: data.tabs_content[name].sections
		}
	}

	getSelectedTab(){
		return (this.state && this.state.selectedTab) || data.tabs[0];
	}
	
	changeTab(name){
		//update the state from the data on this particular tab
		this.setState({
			selectedTab: name,
			sections: data.tabs_content[name].sections
		});
	}
	
  render() {
    return <div className="App">
    
    	<div className="Tabs">
    		<ul className="Tabs_list">
    		
    			{data.tabs.map((tabname, i) =>
						<li key={i} className={(this.getSelectedTab() == tabname ? 'active' : null) + " Tabs_text"} 
							onClick={this.changeTab.bind(this, tabname)}>
							{tabname}
						</li>
    			)}
    			
    		</ul>
    	</div>
    	<div className="clearfix"></div>
    	<div className="Content">
    		<div className="Content_body row no_row_style">
    		
    			{this.state.sections.map((entry, i) =>    		
						<div className={entry.section_class + ' no_row_style'} key={i}>
							<Section section={entry}/>
						</div>
					)}
					
    		</div>
    	</div>
    	
		</div>;
  }
}

export default App;
