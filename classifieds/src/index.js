import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';

//The NYTimes Classifieds is a subcomponent of a much larger page
//So we simulate a widget environment here
ReactDOM.render(
	<div className="external">
		<div className="external_container">
			<App />
		</div>
		<p className="external_text">Feel free to resize the window for mobile viewing</p>
	</div>, 
	document.getElementById('root')
);
