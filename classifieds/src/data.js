let data = {
	tabs: [
		"Real Estate", "Autos", "Jobs", "All Classifieds"
	],
	tabs_content: {
		"Real Estate": {
			sections: [
				{
					section_type: 'layout1',
					section_class: 'col-sm-7',
					section_title: 'International Real Estate',
					title: 'House Hunting in London',
					image: 'house1.jpg',
					post: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat ipsum at turpis ultrices pretium.',
					link: '#'
				},
				{
					section_type: 'layout2',
					section_class: 'col-sm-5',
					section_title: 'Find Properties',
					links: [
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Duis quam turpis',
							link: '#'
						},
						{
							text: 'molestie et rutrum vel',
							link: '#'
						},
						{
							text: 'ullamcorper vel turpis',
							link: '#'
						}
					]
				},
				{
					section_type: 'break'
				},
				{
					section_type: 'layout3',
					section_class: 'col-sm-7',
					image: 'house2.jpg',
					link: '#'
				},
				{
					section_type: 'layout4',
					section_class: 'col-sm-5',
					image: 'house1.jpg',
					links: [
						{
							text: 'Curabitur sed leo orci',
							link: '#'
						},
						{
							text: 'Sed ac porta libero',
							link: '#'
						},
						{
							text: 'Suspendisse malesuada',
							link: '#'
						},
					]
				},
			]
		},
		"Autos": {
			sections: [
				{
					section_type: 'layout1',
					section_class: 'col-sm-6',
					section_title: 'International Auto',
					title: 'Luxury Vehicles',
					image: 'auto1.jpg',
					post: 'Proin quis orci ut tortor tristique iaculis. Sed quam urna, commodo quis erat vitae.',
					link: '#'
				},
				{
					section_type: 'layout2',
					section_class: 'col-sm-6',
					section_title: 'Find Vehicles',
					links: [
						{
							text: 'Go to Vehicle Station',
							link: '#'
						},
						{
							text: 'blandit gravida ex',
							link: '#'
						},
					]
				},
				{
					section_type: 'break'
				},
				{
					section_type: 'layout1',
					section_class: 'col-sm-6',
					title: 'Super Sport Cars',
					image: 'auto2.jpg',
					post: 'Nulla efficitur vehicula egestas. Sed lobortis nibh id tortor feugiat interdum.',
					link: '#'
				},
				{
					section_type: 'layout1',
					section_class: 'col-sm-6',
					title: 'Super Sport Cars #2',
					image: 'auto2.jpg',
					post: 'Nulla efficitur vehicula egestas. Sed lobortis nibh id tortor feugiat interdum.',
					link: '#'
				},
			]
		},
		"Jobs": {
			sections: [
				{
					section_type: 'layout1',
					section_class: 'col-sm-7',
					section_title: 'Work Near You',
					title: 'Jobs in London',
					image: 'job1.jpg',
					post: 'Nulla efficitur vehicula egestas. Sed lobortis nibh id tortor feugiat interdum.',
					link: '#'
				},
				{
					section_type: 'layout2',
					section_class: 'col-sm-5',
					section_title: 'Find Work',
					links: [
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						}
					]
				},
				{
					section_type: 'break'
				},
				{
					section_type: 'layout3',
					section_class: 'col-sm-7',
					image: 'job2.jpg',
					link: '#'
				},
				{
					section_type: 'layout4',
					section_class: 'col-sm-5',
					image: 'job3.jpg',
					links: [
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						}
					]
				},
			]
		},
		"All Classifieds": {
			sections: [
				{
					section_type: 'layout1',
					section_class: 'col-sm-7',
					section_title: 'International Real Estate',
					title: 'House Hunting in London',
					image: 'house1.jpg',
					post: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat ipsum at turpis ultrices pretium.',
					link: '#'
				},
				{
					section_type: 'layout2',
					section_class: 'col-sm-5',
					section_title: 'Find Work',
					links: [
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						}
					]
				},
				{
					section_type: 'break'
				},
				{
					section_type: 'layout3',
					section_class: 'col-sm-7',
					image: 'job2.jpg',
					link: '#'
				},
				{
					section_type: 'layout4',
					section_class: 'col-sm-5',
					image: 'job3.jpg',
					links: [
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						},
						{
							text: 'Go to Real Estate Station',
							link: '#'
						}
					]
				},
			]
		}
	}
};

export default data;
