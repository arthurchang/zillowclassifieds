import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, mount} from 'enzyme';
import {List, Map} from 'immutable';
import Section from '../../src/components/Section';
import {expect} from 'chai';

describe('Section', () => {

  it('renders section with section title', () => {
    const section = {
    	section_type: 'layout1',
    	section_title: 'Find Homes',
    	
    };
    const component = mount(
      <Section section={section} />
    );
    expect(component.find('h5').length).to.equal(1);
    expect(component.find('h5').text()).to.contain('Find Homes');
  });
  
  it('renders section with no section title', () => {
    const section = {
    	section_type: 'layout1',
    	
    };
    const component = mount(
      <Section section={section} />
    );
    expect(component.find('h5').length).to.equal(0);
  });
  
  it('renders section with layout 1', () => {
    const section = {
    	section_type: 'layout1',
    	title: 'Car',
    	post: 'Boat',
    	image: 'Train.jpg'
    };
    const component = mount(
      <Section section={section} />
    );
    expect(component.find('h1').length).to.equal(1);
    expect(component.find('h1').text()).to.contain('Car');
    expect(component.find('p').length).to.equal(1);
    expect(component.find('p').text()).to.contain('Boat');
    expect(component.find('img').length).to.equal(1);
    expect(component.find('img').get(0).props.src).to.contain('Train.jpg');
  });
  
  it('renders section with layout 2 with incorrect data', () => {
    const section = {
    	section_type: 'layout2',
    	title: 'Car',
    	post: 'Boat',
    	image: 'Train.jpg'
    };
    const component = mount(
      <Section section={section} />
    );
    expect(component.find('h1').length).to.equal(0);
    expect(component.find('p').length).to.equal(0);
    expect(component.find('img').length).to.equal(0);
  });
  
  it('renders section with layout 2', () => {
    const section = {
    	section_type: 'layout2',
    	title: 'Car',
    	post: 'Boat',
    	image: 'Train.jpg',
    	links: [{
    		text: 'Hi',
    		link: '#'
    	}]
    };
    const component = mount(
      <Section section={section} />
    );
    expect(component.find('a').length).to.equal(1);
    expect(component.find('a').text()).to.contain("Hi");
  });
  
});