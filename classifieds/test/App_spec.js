import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, mount} from 'enzyme';
import {expect} from 'chai';
import App from './../src/App';

describe('App', () => {

	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<App />, div);
	});
	
	it('renders all 4 tabs', () => {
    const component = mount(
      <App />
    );
    expect(component.find('.Tabs_list li').length).to.equal(4);
    expect(component.find('.Tabs_list li').at(0).text()).to.contain('Real Estate');
    expect(component.find('.Tabs_list li').at(1).text()).to.contain('Auto');
		expect(component.find('.Tabs_list li').at(2).text()).to.contain('Jobs');
		expect(component.find('.Tabs_list li').at(3).text()).to.contain('All');
  });
  
  it('check initial active tab', () => {
    const component = mount(
      <App />
    );
    expect(component.find('.Tabs_list li').length).to.equal(4);
    expect(component.find('.Tabs_list li').at(0).hasClass('active')).to.equal(true);
    expect(component.find('.Tabs_list li').at(1).hasClass('active')).to.equal(false);
    expect(component.find('.Tabs_list li').at(2).hasClass('active')).to.equal(false);
    expect(component.find('.Tabs_list li').at(3).hasClass('active')).to.equal(false);
  });
  
  it('check initial active tab', () => {
    const component = mount(
      <App />
    );
    expect(component.find('.Tabs_list li').length).to.equal(4);
    
   	component.find('.Tabs_list li').at(2).simulate('click', 1);
    
    expect(component.find('.Tabs_list li').at(0).hasClass('active')).to.equal(false);
    expect(component.find('.Tabs_list li').at(1).hasClass('active')).to.equal(false);
    expect(component.find('.Tabs_list li').at(2).hasClass('active')).to.equal(true);
    expect(component.find('.Tabs_list li').at(3).hasClass('active')).to.equal(false);
  });
  
  
});