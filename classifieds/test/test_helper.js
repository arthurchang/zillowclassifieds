import { JSDOM } from "jsdom";
import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import Adapter from 'enzyme-adapter-react-16';
import {configure} from 'enzyme';

function setUpDomEnvironment(){
	const dom = new JSDOM('<!doctype html><html><body></body></html>');
	const {window} = dom;

	global.window = window;
	global.document = window.document;
	global.navigator = {
		userAgent: 'node.js',
	};
	
	global.$ = require('jquery');
	global.jQuery = require('jquery');
	
	copyProps(window, global);
}

function copyProps(src, target){
	const props = Object.getOwnPropertyNames(src)
		.filter(prop => typeof target[prop] === 'undefined')
		.map(prop => Object.getOwnPropertyDescriptor(src, prop));
	Object.defineProperties(target, props);
}

setUpDomEnvironment();

function noop(){
	return {};
}

require.extensions['.css'] = noop;
require.extensions['.svg'] = noop;

configure({ adapter: new Adapter() });


